#[compute]

#version 450


layout(local_size_x = 10, local_size_y = 1, local_size_z = 1) in;

// The restrict keyword tells the shader that this buffer is only going to be accessed from one place in this shader
layout(set = 0, binding = 0, std430) restrict buffer PositionsBuffer {
    vec3 pos[];
}
positions;

layout(set = 0, binding = 1, std430) restrict buffer VelocitiesBuffer {
    vec3 vel[];
}
velocities;

void main() {
    uint index = gl_GlobalInvocationID.x;
    positions.pos[index] += velocities.vel[index];
    //positions.pos[index].xyz = vec3(index * 10 + 1, index * 10 + 2, index * 10 + 3);
}
