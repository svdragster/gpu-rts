extends Node

var rd : RenderingDevice

var input_positions : PackedFloat32Array
var position_vectors : Array
var input_velocities : PackedFloat32Array
var positions_buffer : RID
var velocities_buffer : RID

var uniform_set : RID

var pipeline : RID

var unit_amount := 22000
var unit_positions := []
var unit_velocities := []

var group_size_x : int = ceili(unit_amount / 10)

var time := 0.0

var thread : Thread
var mutex : Mutex

# Called when the node enters the scene tree for the first time.
func _ready():
	
	thread = Thread.new()
	mutex = Mutex.new()
	
	print("group_size_x: ", group_size_x)
	
	var units = get_node("/root/World/units")
	for unit_index in range(unit_amount):
		#var x_pos = unit_data[unit_index]
		#var z_pos = unit_data[unit_index + unit_amount]
		#var x_vel = unit_data[unit_index + unit_amount*2]
		#var z_vel = unit_data[unit_index + unit_amount*3]
		var unit = preload("res://unit.tscn").instantiate()
		unit.position = Vector3(((unit_index % 100)) * 0.2 - 5, 0, ((unit_index) / 100) * 0.2 - 5)
		units.add_child(unit)
		unit.get_node("Label3D").text = str(unit_index)
		
		unit_positions.append(unit.position.x)
		unit_positions.append(unit.position.y)
		unit_positions.append(unit.position.z)
		unit_positions.append(123)
		#unit_velocities.append(Vector3(0.1, 0, 0.2))
		unit_velocities.append(0.05)
		unit_velocities.append(0)
		unit_velocities.append(0.05)
		unit_velocities.append(0)
		
		position_vectors.append(Vector3(unit.position))
	
	
	# Create a local rendering device.
	rd = RenderingServer.create_local_rendering_device()
	
	# Load GLSL shader
	var shader_file := load("res://unit_compute.glsl")
	var shader_spirv: RDShaderSPIRV = shader_file.get_spirv()
	var shader := rd.shader_create_from_spirv(shader_spirv)
	
	# Prepare our data. We use floats in the shader, so we need 32 bit.
	
	input_positions = PackedFloat32Array(unit_positions)
	var input_positions_bytes := input_positions.to_byte_array()

	input_velocities = PackedFloat32Array(unit_velocities)
	var input_velocities_bytes := input_velocities.to_byte_array()

	# Create a storage buffer that can hold our float values.
	positions_buffer = rd.storage_buffer_create(input_positions_bytes.size(), input_positions_bytes)
	velocities_buffer = rd.storage_buffer_create(input_velocities_bytes.size(), input_velocities_bytes)
	
	# Create a uniform to assign the buffer to the rendering device
	var positions_uniform := RDUniform.new()
	positions_uniform.uniform_type = RenderingDevice.UNIFORM_TYPE_STORAGE_BUFFER
	positions_uniform.binding = 0  # this needs to match the "binding" in our shader file
	positions_uniform.add_id(positions_buffer)
	
	var velocities_uniform := RDUniform.new()
	velocities_uniform.uniform_type = RenderingDevice.UNIFORM_TYPE_STORAGE_BUFFER
	velocities_uniform.binding = 1  # this needs to match the "binding" in our shader file
	velocities_uniform.add_id(velocities_buffer)
	
	uniform_set = rd.uniform_set_create([positions_uniform, velocities_uniform], shader, 0)  # the last parameter (the 0) needs to match the "set" in our shader file
	
	# Create a compute pipeline
	pipeline = rd.compute_pipeline_create(shader)
	var compute_list := rd.compute_list_begin()
	rd.compute_list_bind_compute_pipeline(compute_list, pipeline)
	rd.compute_list_bind_uniform_set(compute_list, uniform_set, 0)
	rd.compute_list_dispatch(compute_list, group_size_x, 1, 1)
	rd.compute_list_end()
	
	thread.start(_thread_function)
	
	set_physics_process(true)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var start := Time.get_ticks_msec()
	mutex.lock()
	var i := 0
	for unit in get_node("/root/World/units").get_children():
		unit.position = position_vectors[i]
		i += 1
	mutex.unlock()
	var diff := Time.get_ticks_msec() - start
	print("Time _process: ", diff)
		
	
func _thread_function():
	print("Thread started")
	OS.delay_msec(2000)
	while true:
		var start = Time.get_ticks_msec()
		update_positions()
		var sleep_time = 30 - (Time.get_ticks_msec() - start)
		if sleep_time < 1:
			sleep_time = 1
		print("sleep_time ", sleep_time)
		OS.delay_msec(sleep_time)
	
func _exit_tree():
	thread.wait_to_finish()

func update_positions():
	var input_positions_bytes = input_positions.to_byte_array()
	rd.buffer_update(positions_buffer, 0, input_positions_bytes.size(), input_positions_bytes)
	
	var compute_list := rd.compute_list_begin()
	rd.compute_list_bind_compute_pipeline(compute_list, pipeline)
	rd.compute_list_bind_uniform_set(compute_list, uniform_set, 0)
	rd.compute_list_dispatch(compute_list, group_size_x, 1, 1)
	rd.compute_list_end()
	
	#var input_bytes := rd.buffer_get_data(positions_buffer)
	#var input := input_bytes.to_float32_array()
	
	#print("Input: ", input)
	
	var start_gpu = Time.get_ticks_msec()
	
	# Submit to GPU and wait for sync
	rd.submit()
	rd.sync()
	
	var diff_gpu = Time.get_ticks_msec() - start_gpu
	var start_cpu = Time.get_ticks_msec()
	
	# Read back the data from the buffer
	var output_bytes := rd.buffer_get_data(positions_buffer)
	var output := output_bytes.to_float32_array()
	#print("Output: ", output)
	var i := 0

	var position_vectors_safe := Array(position_vectors)
	position_vectors_safe.resize(unit_amount)

	for c in range(unit_amount):
		var x := output[i]
		input_positions[i] = x
		i += 1
		var y := output[i]
		input_positions[i] = y
		i += 1
		var z := output[i]
		input_positions[i] = z
		i += 2
		position_vectors_safe[c].x = x
		position_vectors_safe[c].y = y
		position_vectors_safe[c].z = z
		
	mutex.lock()
	#input_positions_copy = PackedFloat32Array(input_positions)
	position_vectors = position_vectors_safe
	mutex.unlock()
	
	var diff_cpu = Time.get_ticks_msec() - start_cpu
	
	print("Time gpu: ", diff_gpu)
	print("Time cpu: ", diff_cpu)
